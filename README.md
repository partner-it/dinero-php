# Dinero PHP Wrapper

[![Codeship Status for partner-it/dinero-php](https://codeship.com/projects/488ce7a0-1343-0133-7575-225b415ea37b/status?branch=master)](https://codeship.com/projects/92743)

## Installation

Using Composer

```
composer require partner-it/dinero-php
```

## Usage

Create a new instance:

```
<?php

$Dinero = new \PartnerIT\Dinero\Dinero([
			'clientId'     => 'clientId',
			'clientSecret' => 'xxx',
			'apiKey'       => 'yyy'
		]);

```

Request a new token:
```
<?php
$Dinero->requestToken();
```

Get the Organizations
```
<?php
$organizations = $Dinero->Organizations->getOrganizations();
$organizationId = $organizations[0]['id'];
```

Create a new ledger:

```
<?php

$nextVoucherNumber = $Dinero->LedgerItems->getNextVoucherNumber($organizationId);
$data = [
	[
		"Amount" => -500,
		"Description" => "produkt, lille",
		"BalancingAccountVatCode" => null,
		"AccountNumber" => 1000,
		"BalancingAccountNumber" => null,
		"VoucherDate" => "2015-05-27",
		"VoucherNumber" => $nextVoucherNumber,
		"AccountVatCode" => "I25"
	]
];

$Dinero->LedgerItems->createLedgeritems($organizationId, $data);
```


Run a manual call:

```
<?php

$result = $Dinero->call('/endpoint', 'GET', []);

```

## Links

Official API documentation:

https://api.dinero.dk/docs