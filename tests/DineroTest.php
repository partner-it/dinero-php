<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 22/07/15
 * Time: 09:11
 */
class DineroTest extends PHPUnit_Framework_TestCase
{

	public function testInit()
	{
		$Dinero = new \PartnerIT\Dinero\Dinero([
			'clientId'     => 'clientId',
			'clientSecret' => 'secret',
			'apiKey'       => 'apikey'
		]);

		$this->assertInstanceOf('\\PartnerIT\Dinero\\Dinero', $Dinero);

	}

	public function testTokenHeader()
	{
		$Dinero = new \PartnerIT\Dinero\Dinero([
			'clientId'     => 'clientId',
			'clientSecret' => 'secret',
			'apiKey'       => 'apikey'
		]);

		$result = $Dinero->buildBasicAuthHeader();
		$this->assertEquals('Authorization: Basic Y2xpZW50SWQ6c2VjcmV0', $result);

	}

	public function testHeaders()
	{
		$Dinero = new \PartnerIT\Dinero\Dinero([
			'clientId'     => 'clientId',
			'clientSecret' => 'secret',
			'apiKey'       => 'apikey'
		]);

		$Dinero->setToken('aToken');
		$result = $Dinero->buildTokenAuthHeader();
		$this->assertEquals('Authorization: Bearer aToken', $result);

	}

	/**
	 * @expectedException \Exception
	 * @expectedExceptionMessage You must set or generate a token
	 */
	public function testMissingToken()
	{
		$Dinero = new \PartnerIT\Dinero\Dinero([
			'clientId'     => 'clientId',
			'clientSecret' => 'secret',
			'apiKey'       => 'apikey'
		]);
		$Dinero->call('endpoint', 'GET');
	}

	/**
	 *
	 */
	public function testRequestTokenCurl()
	{

		$stub = $this->getMockBuilder('\\PartnerIT\Curl\\Network\\CurlRequest')
			->getMock();

		// Configure the stub.
		$stub->method('execute')
			->willReturn('foo');

		$stub->method('setOption')
			->willReturn(true);

		$stub->method('getErrorNo')
			->willReturn(0);

		$stub->method('getInfo')
			->will($this->returnValueMap([
				[CURLINFO_HTTP_CODE, 200],
			]));

		$Dinero = new \PartnerIT\Dinero\Dinero([
			'clientId'     => 'clientId',
			'clientSecret' => 'secret',
			'apiKey'       => 'apikey'
		], $stub);

		$Dinero->requestToken();

	}

	/**
	 * @expectedException \RuntimeException
	 * @expectedExceptionMessage Failed to get token
	 */
	public function testRequestTokenCurlFailure()
	{

		$stub = $this->getMockBuilder('\\PartnerIT\Curl\\Network\\CurlRequest')
			->getMock();

		// Configure the stub.
		$stub->method('execute')
			->willReturn('foo');

		$stub->method('setOption')
			->willReturn(true);

		$stub->method('getErrorNo')
			->willReturn(0);

		$stub->method('getInfo')
			->will($this->returnValueMap([
				[CURLINFO_HTTP_CODE, 401],
			]));

		$Dinero = new \PartnerIT\Dinero\Dinero([
			'clientId'     => 'clientId',
			'clientSecret' => 'secret',
			'apiKey'       => 'apikey'
		], $stub);

		$Dinero->requestToken();

	}


	public function testRequestCall()
	{

		$stub = $this->getMockBuilder('\\PartnerIT\Curl\\Network\\CurlRequest')
			->getMock();


		// Configure the stub.
		$stub->method('execute')
			->willReturn('[{"name":"PARTNER IT ApS"}]');

		$stub->method('setOption')
			->willReturn(true);

		$stub->method('getErrorNo')
			->willReturn(0);

		$stub->method('getInfo')
			->will($this->returnValueMap([
				[CURLINFO_HTTP_CODE, 200],
			]));

		$Dinero = new \PartnerIT\Dinero\Dinero([
			'clientId'     => 'clientId',
			'clientSecret' => 'secret',
			'apiKey'       => 'apikey'
		], $stub);

		$Dinero->setToken('atoke');
		$resulst = $Dinero->call('myendoint', 'GET');

		$this->assertEquals(200, $resulst['statusCode']);

		$this->assertEquals([
			['name' => 'PARTNER IT ApS']
		], $resulst['responseBody']);

	}

	/**
	 * @expectedException \RuntimeException
	 */
	public function testRequestFailedCall()
	{

		$stub = $this->getMockBuilder('\\PartnerIT\Curl\\Network\\CurlRequest')
			->getMock();

		// Configure the stub.
		$stub->method('execute')
			->willReturn('foo');

		$stub->method('setOption')
			->willReturn(true);

		$stub->method('getErrorNo')
			->willReturn(0);

		$stub->method('getInfo')
			->will($this->returnValueMap([
				[CURLINFO_HTTP_CODE, 401],
			]));

		$Dinero = new \PartnerIT\Dinero\Dinero([
			'clientId'     => 'clientId',
			'clientSecret' => 'secret',
			'apiKey'       => 'apikey'
		], $stub);

		$Dinero->setToken('atoke');
		$Dinero->call('myendoint', 'GET');

	}

}
