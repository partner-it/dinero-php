<?php

namespace PartnerIT\Dinero;

use PartnerIT\Curl\Network\CurlRequest;

/**
 * Class Dinero
 * @package PartnerIT\Dinero
 */
class Dinero
{

	protected $_baseUri = 'https://api.dinero.dk/v1';

	protected $_apiKey = null;
	protected $_token = null;
	protected $_clientSecret = null;

	/**
	 * @var Organizations
	 */
	public $Organizations;

	/**
	 * @var LedgerItems
	 */
	public $LedgerItems;

	/**
	 * @var CurlRequest
	 */
	private $CurlRequest;

	/**
	 * @param array $options
	 * @throws \Exception
	 */
	public function __construct($options = [], CurlRequest $curlRequest = null)
	{

		if (isset($options['clientId']) && isset($options['clientSecret']) && isset($options['apiKey'])) {

			$this->_clientId = $options['clientId'];
			$this->_clientSecret = $options['clientSecret'];
			$this->_apiKey = $options['apiKey'];

			$this->Organizations = new Organizations($this);
			$this->LedgerItems = new LedgerItems($this);

			if (!$curlRequest) {
				$this->CurlRequest = new CurlRequest();
			} else {
				$this->CurlRequest = $curlRequest;
			}

		} else {
			throw new \InvalidArgumentException('need apiKey, clientId and clientSecret');
		}
	}

	/**
	 * @param $token
	 */
	public function setToken($token)
	{
		$this->_token = $token;
	}

	/**
	 *
	 */
	public function requestToken()
	{

		$results = $this->curl('https://authz.dinero.dk/dineroapi/oauth/token', 'POST', [
			$this->buildBasicAuthHeader(),
			"Content-Type: application/x-www-form-urlencoded",
		], [
			"username"   => $this->_apiKey,
			"scope"      => "read write",
			"password"   => $this->_apiKey,
			"grant_type" => "password"
		], $json = false);

		if ($results['statusCode'] === 200) {
			$this->setToken($results['responseBody']['access_token']);
		} else {
			throw new \RuntimeException('Failed to get token: '. $results['responseBody']['message']);
		}

	}

	/**
	 * @param $endpoint
	 * @param $method
	 * @param array $data
	 * @return mixed
	 * @throws \Exception
	 */
	public function call($endpoint, $method, $data = [])
	{

		if (empty($this->_token)) {
			throw new \Exception('You must set or generate a token');
		}

		$headers = [
			$this->buildTokenAuthHeader()
		];

		$result = $this->curl($this->_baseUri . '/' . $endpoint, $method, $headers, $data);

		if ($result['statusCode'] >= 200 && $result['statusCode'] < 300) {
			return $result;
		}

		if ($result['statusCode'] >= 400) {
			throw new \RuntimeException('Error' . $result['statusCode'] . ' - ' . $result['responseBody']);
		}

	}

	/**
	 * @param $uri
	 * @param string $method
	 * @param array $headers
	 * @param array $body
	 * @return array
	 * @throws \Exception
	 */
	private function curl($uri, $method = 'GET', $headers = [], $body = [], $json = true)
	{

		$this->CurlRequest->setOption(CURLOPT_URL, $uri);
		$this->CurlRequest->setOption(CURLOPT_CUSTOMREQUEST, $method);
		$this->CurlRequest->setOption(CURLOPT_RETURNTRANSFER, 1);

		if ($json) {
			$body = json_encode($body);
			$headers[] = 'Content-Type: application/json';
		} else {
			$body = http_build_query($body);
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
		}

		$this->CurlRequest->setOption(CURLOPT_POST, 1);
		$this->CurlRequest->setOption(CURLOPT_POSTFIELDS, $body);
		$this->CurlRequest->setOption(CURLOPT_HTTPHEADER, $headers);

		$resp = $this->CurlRequest->execute();
		if ($this->CurlRequest->getErrorNo() !== 0) {
			throw new \RuntimeException('curl error ' . $this->CurlRequest->getError() . '" - Code: ' . $this->CurlRequest->getErrorNo());
		} else {
			return ['statusCode'   => $this->CurlRequest->getInfo(CURLINFO_HTTP_CODE),
					'responseBody' => json_decode($resp, true)
			];
		}
	}

	/**
	 * @return string
	 */
	public function buildBasicAuthHeader()
	{
		return 'Authorization: Basic ' . base64_encode($this->_clientId . ':' . $this->_clientSecret);
	}

	/**
	 * @return string
	 */
	public function buildTokenAuthHeader()
	{
		return 'Authorization: Bearer ' . $this->_token;
	}

}
