<?php

namespace PartnerIT\Dinero;

/**
 * Class Organizations
 * @package PartnerIT\Dinero
 */
class Organizations
{

	/**
	 * @var Dinero
	 */
	private $Dinero;

	/**
	 * @param Dinero $Dinero
	 */
	public function __construct(Dinero $Dinero)
	{
		$this->Dinero = $Dinero;
	}

	/**
	 *
	 */
	public function getOrganizations()
	{
		$response = $this->Dinero->call('organizations?fields=name,id,isPro', 'GET');

		return $response['responseBody'];

	}

}
