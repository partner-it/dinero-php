<?php

namespace PartnerIT\Dinero;

/**
 * Class Organizations
 * @package PartnerIT\Dinero
 */
class LedgerItems
{

	/**
	 * @var Dinero
	 */
	private $Dinero;

	/**
	 * @param Dinero $Dinero
	 */
	public function __construct(Dinero $Dinero)
	{
		$this->Dinero = $Dinero;
	}

	/**
	 * @param $organizationId
	 * @return mixed
	 * @throws \Exception
	 */
	public function getNextVoucherNumber($organizationId)
	{
		$response = $this->Dinero->call($organizationId . '/ledgeritems/nextvouchernumber', 'GET');

		return $response['responseBody']['NextVoucherNumber'];
	}

	/**
	 * @param $organizationId
	 * @param $data
	 * @return bool
	 * @throws \Exception
	 */
	public function createLedgeritems($organizationId, $data)
	{
		$this->Dinero->call($organizationId . '/ledgeritems', 'POST', $data);
		return true;
	}

}
